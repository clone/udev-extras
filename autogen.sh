#!/bin/sh -e

(gtkdocize --flavour no-tmpl) < /dev/null > /dev/null 2>&1 || {
        echo
        echo "You must have gtk-doc installed to compile udev-extras."
        echo "Install the appropriate package for your distribution,"
        echo "or get the source tarball at http://ftp.gnome.org/pub/GNOME/sources/gtk-doc/"
        exit 1
}

autoreconf --install --symlink

CFLAGS="-g -Wall \
-Wmissing-declarations -Wmissing-prototypes \
-Wnested-externs -Wpointer-arith \
-Wpointer-arith -Wsign-compare -Wchar-subscripts \
-Wstrict-prototypes -Wshadow \
-Wformat=2 -Wtype-limits"
args="--prefix=/usr --exec-prefix= --enable-gtk-doc"

case "$1" in
	*install|"")
                shift
                args="$args $@"
		export CFLAGS="$CFLAGS -O2"
		echo "   configure:  $args"
		echo
		./configure $args
		;;
	*clean)
		./configure
		make maintainer-clean
		git clean -f -X
		exit 0
		;;
	*)
		echo "Usage: $0 [--install|--devel|--clean]"
		exit 1
		;;
esac
